/*
 * libuci - Library for the Unified Configuration Interface
 * Copyright (C) 2016 Matthias Schiffer <mschiffer@universe-factory.net>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License version 2.1
 * as published by the Free Software Foundation
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 */


#define _GNU_SOURCE

#include "uci.h"
#include "uci_internal.h"

#include <libece.h>
#include <libubus.h>

#include <unistd.h>


#define ECE_UCI_PATH "/lib/ece/uci/"


struct uci_ece_priv {
	struct ubus_context *ubus;
};


static struct blob_attr * uci_ece_blob_load(const char *filename) {
	FILE *f = fopen(filename, "rb");
	if (!f)
		return NULL;

	struct blob_attr head;
	if (fread(&head, sizeof(head), 1, f) != 1) {
		fclose(f);
		return NULL;
	}

	size_t size = blob_pad_len(&head);
	struct blob_attr *ret = malloc(size);
	if (!ret) {
		fclose(f);
		return NULL;
	}

	memcpy(ret, &head, sizeof(head));
	if (fread(blob_data(ret), size - sizeof(head), 1, f) != 1) {
		fclose(f);
		free(ret);
		return NULL;
	}

	if (fclose(f)) {
		free(ret);
		return NULL;
	}

	return ret;
}

static void uci_ece_set_list(struct uci_context *ctx, struct uci_ptr *ptr, struct blob_attr *value) {
	char buf[11];

	struct blob_attr *cur;
	ssize_t rem;
	blobmsg_for_each_attr(cur, value, rem) {
		switch (blobmsg_type(cur)) {
		case BLOBMSG_TYPE_STRING:
			ptr->value = blobmsg_get_string(cur);
			break;

		case BLOBMSG_TYPE_INT32:
			snprintf(buf, sizeof(buf), "%i", (int)(int32_t)blobmsg_get_u32(cur));
			ptr->value = buf;
			break;

		case BLOBMSG_TYPE_BOOL:
			snprintf(buf, sizeof(buf), "%i", (int)blobmsg_get_bool(cur));
			ptr->value = buf;
			break;

		default:
			continue;
		}

		UCI_NESTED(uci_add_list, ctx, ptr);
	}
}

static void uci_ece_set_option(struct uci_context *ctx, struct uci_package *package, struct uci_section *section, const char *option, struct blob_attr *value) {
	char buf[11];

	struct uci_ptr ptr = {};
	ptr.s = section;
	ptr.section = section->e.name;
	ptr.p = package;
	ptr.package = package->e.name;
	ptr.option = option;
	ptr.flags = UCI_LOOKUP_DONE;

	switch (blobmsg_type(value)) {
	case BLOBMSG_TYPE_STRING:
		ptr.value = blobmsg_get_string(value);
		break;

	case BLOBMSG_TYPE_INT32:
		snprintf(buf, sizeof(buf), "%i", (int)(int32_t)blobmsg_get_u32(value));
		ptr.value = buf;
		break;

	case BLOBMSG_TYPE_BOOL:
		snprintf(buf, sizeof(buf), "%i", (int)blobmsg_get_bool(value));
		ptr.value = buf;
		break;

	case BLOBMSG_TYPE_ARRAY:
		uci_ece_set_list(ctx, &ptr, value);
		return;

	default:
		return;
	}

	UCI_NESTED(uci_set, ctx, &ptr);
}

static void uci_ece_import_option(struct uci_context *ctx, struct uci_package *package, struct uci_section *section, struct blob_attr *blob) {
	if (!blobmsg_check_attr_list(blob, BLOBMSG_TYPE_UNSPEC))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	const char *option = blobmsg_name(blob);

	struct uci_ece_priv *priv = ctx->backend->priv;

	const struct blobmsg_policy policy = { .name = "path", .type = BLOBMSG_TYPE_STRING };
	struct blob_attr *attr;
	if (blobmsg_parse(&policy, 1, &attr, blobmsg_data(blob), blobmsg_data_len(blob)))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	if (!attr)
		UCI_THROW(ctx, UCI_ERR_PARSE);

	const char *path = blobmsg_get_string(attr);

	struct blob_attr *value = libece_get(priv->ubus, path, NULL);
	if (!value)
		return;

	const struct blobmsg_policy value_policy = { .name = "value", .type = BLOBMSG_TYPE_UNSPEC };
	struct blob_attr *value_attr;
	if (blobmsg_parse(&value_policy, 1, &value_attr, blob_data(value), blob_len(value)) == 0 && value_attr)
		uci_ece_set_option(ctx, package, section, option, value_attr);

	free(value);
}

static void uci_ece_import_static_section(struct uci_context *ctx, struct uci_package *package, struct blob_attr *blob, const char *type, const char *name) {
	if (!blobmsg_check_attr_list(blob, BLOBMSG_TYPE_TABLE))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	struct uci_section *section;

	if (name) {
		struct uci_ptr ptr = {};
		ptr.section = name;
		ptr.p = package;
		ptr.package = package->e.name;
		ptr.value = type;
		ptr.flags = UCI_LOOKUP_DONE;

		UCI_NESTED(uci_set, ctx, &ptr);
		section = uci_to_section(ptr.last);
	}
	else {
		uci_add_section(ctx, package, type, &section);
	}

	struct blob_attr *cur;
	ssize_t rem;
	blobmsg_for_each_attr(cur, blob, rem)
		uci_ece_import_option(ctx, package, section, cur);

	uci_fixup_section(ctx, section);
}

static void uci_ece_import_static_type(struct uci_context *ctx, struct uci_package *package, struct blob_attr *blob) {
	if (!blobmsg_check_attr_list(blob, BLOBMSG_TYPE_TABLE))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	const char *type = blobmsg_name(blob);
	bool named = blobmsg_type(blob) == BLOBMSG_TYPE_TABLE;

	struct blob_attr *cur;
	ssize_t rem;
	blobmsg_for_each_attr(cur, blob, rem)
		uci_ece_import_static_section(ctx, package, cur, type, named ? blobmsg_name(cur) : NULL);
}

static void uci_ece_import_static(struct uci_context *ctx, struct uci_package *package, struct blob_attr *blob) {
	if (!blobmsg_check_attr_list(blob, BLOBMSG_TYPE_UNSPEC))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	struct blob_attr *cur;
	ssize_t rem;
	blobmsg_for_each_attr(cur, blob, rem) {
		switch (blobmsg_type(cur)) {
		case BLOBMSG_TYPE_TABLE:
		case BLOBMSG_TYPE_ARRAY:
			uci_ece_import_static_type(ctx, package, cur);
			break;

		default:
			UCI_THROW(ctx, UCI_ERR_PARSE);
		}
	}
}

static bool uci_ece_unescape_path(char *path) {
	const char *in = path;
	char *out = path;

	do {
		if (in[0] == '~') {
			switch (in[1]) {
			case '0':
				*out++ = '~';
				break;

			case '1':
				*out++ = '/';
				break;

			default:
				return false;
			}

			in++;
		}
		else {
			*out++ = in[0];
		}
	} while (*in++);

	return true;
}

static void uci_ece_import_named_option(struct uci_context *ctx, struct uci_package *package, struct uci_section *section, struct blob_attr *entry, const char *option, const char *path) {
	while (path[0]) {
		const char *prev = path;

		path = strchrnul(path, '/');
		size_t len = path-prev;
		char frag[len+1];
		memcpy(frag, prev, len);
		frag[len] = 0;

		if (*path)
			path++;

		if (!uci_ece_unescape_path(frag))
			return;

		int type = blobmsg_type(entry);

		if (type == BLOBMSG_TYPE_TABLE) {
			const struct blobmsg_policy policy = { .name = frag, .type = BLOBMSG_TYPE_UNSPEC };
			if (blobmsg_parse(&policy, 1, &entry, blobmsg_data(entry), blobmsg_data_len(entry)) || !entry)
				return;
		}
		else if (type == BLOBMSG_TYPE_ARRAY) {
			char *end;
			size_t index = strtoul(frag, &end, 10);
			if (!frag[0] || end[0])
				return;

			struct blob_attr *cur;
			ssize_t rem;
			blobmsg_for_each_attr(cur, entry, rem) {
				if (!index) {
					entry = cur;
					break;
				}

				index--;
			}

			if (index)
				return;
		}
		else {
			return;
		}
	}

	uci_ece_set_option(ctx, package, section, option, entry);
}

static void uci_ece_import_named_section(struct uci_context *ctx, struct uci_package *package, struct blob_attr *entry, struct blob_attr *options, const char *type, const char *base_path) {
	const char *name = blobmsg_name(entry);

	struct uci_section *section;
	struct uci_ptr ptr = {};
	ptr.section = name;
	ptr.p = package;
	ptr.package = package->e.name;
	ptr.value = type;
	ptr.flags = UCI_LOOKUP_DONE;

	UCI_NESTED(uci_set, ctx, &ptr);
	section = uci_to_section(ptr.last);

	struct blob_attr *cur;
	ssize_t rem;
	blobmsg_for_each_attr(cur, options, rem) {
		const struct blobmsg_policy policy = { .name = "path", .type = BLOBMSG_TYPE_STRING };
		struct blob_attr *attr;
		if (blobmsg_parse(&policy, 1, &attr, blobmsg_data(cur), blobmsg_data_len(cur)))
			continue;

		uci_ece_import_named_option(ctx, package, section, entry, blobmsg_name(cur), blobmsg_get_string(attr));
	}
}

static void uci_ece_import_named_type(struct uci_context *ctx, struct uci_package *package, struct blob_attr *blob) {
	const char *type = blobmsg_name(blob);

	struct uci_ece_priv *priv = ctx->backend->priv;

	enum {
		ATTR_PATH,
		ATTR_OPTIONS,
		ATTR_MAX
	};

	const struct blobmsg_policy policy[ATTR_MAX] = {
		{ .name = "path", .type = BLOBMSG_TYPE_STRING },
		{ .name = "options", .type = BLOBMSG_TYPE_TABLE },
	};
	struct blob_attr *attr[ATTR_MAX];
	if (blobmsg_parse(policy, ATTR_MAX, attr, blobmsg_data(blob), blobmsg_data_len(blob)))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	if (!attr[ATTR_PATH] || !attr[ATTR_OPTIONS])
		UCI_THROW(ctx, UCI_ERR_PARSE);

	const char *path = blobmsg_get_string(attr[ATTR_PATH]);

	struct blob_attr *value = libece_get(priv->ubus, path, NULL);
	if (!value)
		return;

	const struct blobmsg_policy value_policy = { .name = "value", .type = BLOBMSG_TYPE_UNSPEC };
	struct blob_attr *value_attr;
	if (blobmsg_parse(&value_policy, 1, &value_attr, blob_data(value), blob_len(value)) == 0 && value_attr) {
		struct blob_attr *cur;
		ssize_t rem;
		blobmsg_for_each_attr(cur, value_attr, rem)
			uci_ece_import_named_section(ctx, package, cur, attr[ATTR_OPTIONS], type, path);
	}

	free(value);
}

static void uci_ece_import_named(struct uci_context *ctx, struct uci_package *package, struct blob_attr *blob) {
	if (!blobmsg_check_attr_list(blob, BLOBMSG_TYPE_TABLE))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	struct blob_attr *cur;
	ssize_t rem;
	blobmsg_for_each_attr(cur, blob, rem)
		uci_ece_import_named_type(ctx, package, cur);
}

static void uci_ece_import_unnamed(struct uci_context *ctx, struct uci_package *package, struct blob_attr *blob) {
}

static void uci_ece_import(struct uci_context *ctx, struct uci_package *package, struct blob_attr *blob, bool flush_delta) {
	if (blobmsg_type(blob) != BLOBMSG_TYPE_TABLE || !blobmsg_check_attr_list(blob, BLOBMSG_TYPE_TABLE))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	enum {
		ATTR_STATIC,
		ATTR_NAMED,
		ATTR_UNNAMED,
		ATTR_MAX
	};

	const struct blobmsg_policy policy[ATTR_MAX] = {
		[ATTR_STATIC] = { .name = "static", .type = BLOBMSG_TYPE_TABLE },
		[ATTR_NAMED] = { .name = "named", .type = BLOBMSG_TYPE_TABLE },
		[ATTR_UNNAMED] = { .name = "unnamed", .type = BLOBMSG_TYPE_TABLE },
	};

	struct blob_attr *attr[ATTR_MAX];
	if (blobmsg_parse(policy, ATTR_MAX, attr, blobmsg_data(blob), blobmsg_data_len(blob)))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	if (attr[ATTR_STATIC])
		uci_ece_import_static(ctx, package, attr[ATTR_STATIC]);
	if (attr[ATTR_NAMED])
		uci_ece_import_named(ctx, package, attr[ATTR_NAMED]);
	if (attr[ATTR_UNNAMED])
		uci_ece_import_unnamed(ctx, package, attr[ATTR_UNNAMED]);

	package->backend = ctx->backend;
	uci_list_add(&ctx->root, &package->e.list);

	package->has_delta = true;
	uci_load_delta(ctx, package, flush_delta);
}

static bool uci_ece_load_mapped(struct uci_context *ctx, const char *name, struct uci_package **package) {
	if (!uci_validate_package(name))
		return false;

	size_t pathlen = strlen(ECE_UCI_PATH);
	size_t namelen = strlen(name);
	char filename[pathlen+namelen+1];
	struct uci_package *p = NULL;

	memcpy(filename, ECE_UCI_PATH, pathlen);
	memcpy(filename+pathlen, name, namelen+1);

	if (access(filename, F_OK) != 0)
		return false;

	ctx->internal = true;

	struct blob_attr *blob = uci_ece_blob_load(filename);
	if (!blob)
		goto err;

	p = uci_alloc_package(ctx, name);

	UCI_TRAP_SAVE(ctx, err);

	uci_ece_import(ctx, p, blob, false);

	UCI_TRAP_RESTORE(ctx);

	*package = p;
	free(blob);

	return true;

err:
	uci_free_package(&p);
	free(blob);
	*package = NULL;

	UCI_THROW(ctx, ctx->err);
}

static struct uci_package * uci_ece_load(struct uci_context *ctx, const char *name) {
	struct uci_package *package;

	if (uci_ece_load_mapped(ctx, name, &package))
		return package;
	else
		return uci_file_backend.load(ctx, name);
}

static bool uci_ece_blobmsg_add_boolean_string(struct blob_buf *b, const char *value) {
	if (
		strcmp(value, "0") == 0 ||
		strcmp(value, "false") == 0 ||
		strcmp(value, "disabled") == 0 ||
		strcmp(value, "off") == 0
	)
		return !blobmsg_add_u8(b, NULL, 0);
	else if (
		strcmp(value, "1") == 0 ||
		strcmp(value, "true") == 0 ||
		strcmp(value, "enabled") == 0 ||
		strcmp(value, "on") == 0
	)
		return !blobmsg_add_u8(b, NULL, 1);
	else
		return false;
}

static bool uci_ece_blobmsg_add_integer_string(struct blob_buf *b, const char *value) {
	errno = 0;
	char *end;
	long i = strtol(value, &end, 0);
	if (!*value || errno != 0 || *end)
		return false;

	return !blobmsg_add_u32(b, NULL, i);
}

static void uci_ece_commit_option(struct uci_context *ctx, struct uci_package *package, struct uci_section *section, struct blob_attr *blob) {
	if (!blobmsg_check_attr_list(blob, BLOBMSG_TYPE_UNSPEC))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	const char *option = blobmsg_name(blob);

	bool found = false;
	struct uci_element *e, *tmp;
	uci_foreach_element_safe(&package->saved_delta, tmp, e) {
		struct uci_delta *h = uci_to_delta(e);
		if (strcmp(h->section, section->e.name) != 0 || !h->e.name || strcmp(h->e.name, option) != 0)
			continue;
		uci_free_delta(h);
		found = true;
	}

	if (!found)
		return;

	bool delete = false;

	size_t plen = strlen(package->e.name);
	size_t slen = strlen(section->e.name);
	size_t olen = strlen(option);
	char buf[plen+1+slen+1+olen+1];
	snprintf(buf, sizeof(buf), "%s.%s.%s", package->e.name, section->e.name, option);
	struct uci_ptr ptr;
	if (uci_lookup_ptr(ctx, &ptr, buf, false))
		delete = true;

	enum {
		ATTR_PATH,
		ATTR_TYPE,
		ATTR_MAX
	};

	const struct blobmsg_policy policy[ATTR_MAX] = {
		[ATTR_PATH] = { .name = "path", .type = BLOBMSG_TYPE_STRING },
		[ATTR_TYPE] = { .name = "type", .type = BLOBMSG_TYPE_STRING },
	};

	struct blob_attr *attr[ATTR_MAX];
	if (blobmsg_parse(policy, ATTR_MAX, attr, blobmsg_data(blob), blobmsg_data_len(blob)))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	if (!attr[ATTR_PATH] || !attr[ATTR_TYPE])
		UCI_THROW(ctx, UCI_ERR_PARSE);

	const char *path = blobmsg_get_string(attr[ATTR_PATH]);
	const char *type = blobmsg_get_string(attr[ATTR_TYPE]);

	struct uci_ece_priv *priv = ctx->backend->priv;
	if (delete) {
		libece_delete(priv->ubus, path);
	}
	else {
		struct blob_buf b = {};
		blob_buf_init(&b, 0);

		bool ok = false;

		if (ptr.o->type == UCI_TYPE_STRING) {
			const char *value = ptr.o->v.string;
			if (strcmp(type, "string") == 0)
				ok = !blobmsg_add_string(&b, NULL, value);
			else if (strcmp(type, "boolean") == 0)
				ok = uci_ece_blobmsg_add_boolean_string(&b, value);
			else if (strcmp(type, "integer") == 0)
				ok = uci_ece_blobmsg_add_integer_string(&b, value);
		}
		else if (ptr.o->type == UCI_TYPE_LIST) {
			struct uci_list *list = &ptr.o->v.list;
			struct uci_element *value;

			void *c = blobmsg_open_array(&b, NULL);
			if (!c)
				goto out;

			if (strcmp(type, "[string]") == 0) {
				uci_foreach_element(list, value) {
					if (blobmsg_add_string(&b, NULL, value->name))
						goto out;
				}

				ok = true;
			}
			else if (strcmp(type, "[boolean]") == 0) {
				uci_foreach_element(list, value) {
					if (!uci_ece_blobmsg_add_boolean_string(&b, value->name))
						goto out;
				}

				ok = true;
			}
			else if (strcmp(type, "[integer]") == 0) {
				uci_foreach_element(list, value) {
					if (!uci_ece_blobmsg_add_integer_string(&b, value->name))
						goto out;
				}

				ok = true;
			}

			blobmsg_close_array(&b, c);
		}

		if (ok)
			libece_set(priv->ubus, path, blob_data(b.head));

	out:
		blob_buf_free(&b);
	}
}

static void uci_ece_commit_static_section(struct uci_context *ctx, struct uci_package *package, struct blob_attr *blob, const char *type, const char *name, unsigned index) {
	if (!blobmsg_check_attr_list(blob, BLOBMSG_TYPE_TABLE))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	size_t plen = strlen(package->e.name);
	size_t nlen = name ? strlen(name) : (strlen(type) + 3 + 20 /* Lets be conservative about the index length */);

	char buf[plen + 1 + nlen + 1];
	if (name)
		snprintf(buf, sizeof(buf), "%s.%s", package->e.name, name);
	else
		snprintf(buf, sizeof(buf), "%s.@%s[%u]", package->e.name, type, index);
	struct uci_ptr ptr;
	if (uci_lookup_ptr(ctx, &ptr, buf, true))
		return;

	struct uci_section *section = ptr.s;

	struct blob_attr *cur;
	ssize_t rem;
	blobmsg_for_each_attr(cur, blob, rem)
		uci_ece_commit_option(ctx, package, section, cur);
}

static void uci_ece_commit_static_type(struct uci_context *ctx, struct uci_package *package, struct blob_attr *blob) {
	if (!blobmsg_check_attr_list(blob, BLOBMSG_TYPE_TABLE))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	const char *type = blobmsg_name(blob);
	bool named = blobmsg_type(blob) == BLOBMSG_TYPE_TABLE;

	struct blob_attr *cur;
	ssize_t rem;
	unsigned index = 0;
	blobmsg_for_each_attr(cur, blob, rem)
		uci_ece_commit_static_section(ctx, package, cur, type, named ? blobmsg_name(cur) : NULL, index++);
}


static void uci_ece_commit_static(struct uci_context *ctx, struct uci_package *package, struct blob_attr *blob) {
	if (!blobmsg_check_attr_list(blob, BLOBMSG_TYPE_UNSPEC))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	struct blob_attr *cur;
	ssize_t rem;
	blobmsg_for_each_attr(cur, blob, rem) {
		switch (blobmsg_type(cur)) {
		case BLOBMSG_TYPE_TABLE:
		case BLOBMSG_TYPE_ARRAY:
			uci_ece_commit_static_type(ctx, package, cur);
			break;

		default:
			UCI_THROW(ctx, UCI_ERR_PARSE);
		}
	}
}

static void uci_ece_commit_named(struct uci_context *ctx, struct uci_package *package, struct blob_attr *blob) {
}

static void uci_ece_commit_unnamed(struct uci_context *ctx, struct uci_package *package, struct blob_attr *blob) {
}

static void uci_ece_commit(struct uci_context *ctx, struct uci_package **package, bool overwrite) {
	struct uci_package *p = *package;

	size_t pathlen = strlen(ECE_UCI_PATH);
	size_t namelen = strlen(p->e.name);
	char filename[pathlen+namelen+1];

	memcpy(filename, ECE_UCI_PATH, pathlen);
	memcpy(filename+pathlen, p->e.name, namelen+1);

	const char *name = filename+pathlen;

	if (access(filename, F_OK) != 0)
		UCI_THROW(ctx, UCI_ERR_INVAL);

	ctx->internal = true;
	int orig_flags = ctx->flags;

	struct blob_attr *blob = uci_ece_blob_load(filename);
	if (!blob)
		goto done;

	if (blobmsg_type(blob) != BLOBMSG_TYPE_TABLE || !blobmsg_check_attr_list(blob, BLOBMSG_TYPE_TABLE))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	enum {
		ATTR_STATIC,
		ATTR_NAMED,
		ATTR_UNNAMED,
		ATTR_MAX
	};

	const struct blobmsg_policy policy[ATTR_MAX] = {
		[ATTR_STATIC] = { .name = "static", .type = BLOBMSG_TYPE_TABLE },
		[ATTR_NAMED] = { .name = "named", .type = BLOBMSG_TYPE_TABLE },
		[ATTR_UNNAMED] = { .name = "unnamed", .type = BLOBMSG_TYPE_TABLE },
	};

	struct blob_attr *attr[ATTR_MAX];
	if (blobmsg_parse(policy, ATTR_MAX, attr, blobmsg_data(blob), blobmsg_data_len(blob)))
		UCI_THROW(ctx, UCI_ERR_PARSE);

	ctx->flags |= UCI_FLAG_SAVED_DELTA;

	if (overwrite) {
		UCI_TRAP_SAVE(ctx, done);
		uci_load_delta(ctx, p, true);
		UCI_TRAP_RESTORE(ctx);
	}
	else {
		UCI_TRAP_SAVE(ctx, done);

		/* dump our own changes to the delta file */
		if (!uci_list_empty(&p->delta))
			UCI_INTERNAL(uci_save, ctx, p);

		UCI_TRAP_RESTORE(ctx);

		uci_free_package(&p);

		UCI_TRAP_SAVE(ctx, done);
		p = uci_alloc_package(ctx, name);
		UCI_TRAP_RESTORE(ctx);

		UCI_TRAP_SAVE(ctx, done);
		uci_ece_import(ctx, p, blob, true);
		UCI_TRAP_RESTORE(ctx);
	}

	ctx->flags = orig_flags;

	UCI_TRAP_SAVE(ctx, done);

	if (attr[ATTR_STATIC])
		uci_ece_commit_static(ctx, p, attr[ATTR_STATIC]);
	if (attr[ATTR_NAMED])
		uci_ece_commit_named(ctx, p, attr[ATTR_NAMED]);
	if (attr[ATTR_UNNAMED])
		uci_ece_commit_unnamed(ctx, p, attr[ATTR_UNNAMED]);

	UCI_TRAP_RESTORE(ctx);

	uci_free_package(&p);

	UCI_TRAP_SAVE(ctx, done);
	p = uci_alloc_package(ctx, name);
	UCI_TRAP_RESTORE(ctx);

	UCI_TRAP_SAVE(ctx, done);
	uci_ece_import(ctx, p, blob, false);
	UCI_TRAP_RESTORE(ctx);

done:
	free(blob);

	ctx->flags = orig_flags;

	*package = p;

	if (ctx->err)
		UCI_THROW(ctx, ctx->err);
}


__private struct uci_backend * uci_ece_init(void) {
	struct uci_ece_priv *priv = malloc(sizeof(*priv));
	if (!priv)
		return NULL;

	priv->ubus = ubus_connect(NULL);
	if (!priv->ubus) {
		free(priv);
		return NULL;
	}

	struct uci_backend *backend = malloc(sizeof(*backend));
	if (!backend) {
		ubus_free(priv->ubus);
		free(priv);
		return NULL;
	}

	UCI_BACKEND(backend_def, "ece",
		.load = uci_ece_load,
		.commit = uci_ece_commit,
		.list_configs = uci_file_backend.list_configs,
		.priv = priv,
	);

	*backend = backend_def;
	return backend;
}

__private void uci_ece_free(struct uci_backend *backend) {
	struct uci_ece_priv *priv = backend->priv;

	ubus_free(priv->ubus);
	free(priv);
	free(backend);
}
